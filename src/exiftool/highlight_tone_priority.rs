use std::fmt;

use serde::de;
use serde::Deserialize;
use serde::Serialize;

#[derive(Clone, Serialize, Deserialize)]
pub enum HighlightTonePriority
{
    Off,
    DPlus1,
    DPlus2,
}

struct HighlightTonePriorityVisitor;

pub(crate) fn deserialize_highlight_tone_priority<'de, D>(d: D) -> Result<HighlightTonePriority, D::Error>
where D: de::Deserializer<'de>
{
    d.deserialize_str(HighlightTonePriorityVisitor)
}

impl<'de> de::Visitor<'de> for HighlightTonePriorityVisitor
{
    type Value = HighlightTonePriority;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result
    {
        write!(formatter, "exiftool HighlightTonePriority tag")
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where E: de::Error
    {
        match value
        {
            "Off" => Ok(HighlightTonePriority::Off),
            "On" => Ok(HighlightTonePriority::DPlus1),
            "Unknown (2)" => Ok(HighlightTonePriority::DPlus2),
            other => Err(E::custom(format!(
                "Unexpected value '{}' in HighlightTonePriority tag.",
                other
            ))),
        }
    }
}
