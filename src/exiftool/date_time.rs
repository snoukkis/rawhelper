use std::fmt;

use chrono::DateTime;
use chrono::FixedOffset;
use chrono::NaiveDateTime;
use chrono::Offset;
use chrono::TimeZone;
use chrono::Utc;
use chrono_tz::Europe::Helsinki;
use serde::de;

pub(crate) fn deserialize_optional_date_time<'de, D>(d: D) -> Result<Option<DateTime<FixedOffset>>, D::Error>
where D: de::Deserializer<'de>
{
    d.deserialize_option(OptionalDateTimeFromCustomFormatVisitor)
}

struct OptionalDateTimeFromCustomFormatVisitor;

impl<'de> de::Visitor<'de> for OptionalDateTimeFromCustomFormatVisitor
{
    type Value = Option<DateTime<FixedOffset>>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result
    {
        write!(formatter, "exiftool formatted string or null")
    }

    fn visit_none<E>(self) -> Result<Self::Value, E>
    where E: de::Error
    {
        Ok(None)
    }

    fn visit_some<D>(self, d: D) -> Result<Option<DateTime<FixedOffset>>, D::Error>
    where D: de::Deserializer<'de>
    {
        Ok(Some(deserialize_date_time(d)?))
    }
}

struct DateTimeFromCustomFormatVisitor;

pub(crate) fn deserialize_date_time<'de, D>(d: D) -> Result<DateTime<FixedOffset>, D::Error>
where D: de::Deserializer<'de>
{
    d.deserialize_str(DateTimeFromCustomFormatVisitor)
}

impl<'de> de::Visitor<'de> for DateTimeFromCustomFormatVisitor
{
    type Value = DateTime<FixedOffset>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result
    {
        write!(formatter, "exiftool formatted string")
    }

    fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
    where E: de::Error
    {
        const FORMATS: &'static [&'static str] = &["%Y:%m:%d %H:%M:%S%.f", "%Y:%m:%d %H:%M:%S"];
        const ZONE: &'static str = "%:z";

        FORMATS
            .iter()
            .find_map(|format_without_zone| {
                // try with zone
                let format_with_zone = format_without_zone.to_string() + ZONE;
                DateTime::parse_from_str(value, &format_with_zone).ok().or_else(|| {
                    // try without zone
                    let naive_date_time = match NaiveDateTime::parse_from_str(value, format_without_zone)
                    {
                        Ok(naive_date_time) => naive_date_time,
                        Err(_) => return None,
                    };

                    let date_time = match Helsinki.from_local_datetime(&naive_date_time)
                    {
                        chrono::LocalResult::None =>
                        {
                            let offset = Utc.fix();
                            DateTime::<FixedOffset>::from_utc(naive_date_time, offset)
                        },
                        chrono::LocalResult::Single(date_time_tz)
                        | chrono::LocalResult::Ambiguous(
                            date_time_tz, /* pick first, this is not for exact science */
                            _,
                        ) =>
                        {
                            let offset = date_time_tz.offset().fix();
                            DateTime::<FixedOffset>::from_utc(date_time_tz.naive_utc(), offset)
                        },
                    };

                    Some(date_time)
                })
            })
            .ok_or(E::custom(format!(
                "Error parsing '{}' as exiftool formatted datetime string",
                value
            )))
    }
}
