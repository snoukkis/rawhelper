pub mod date_time;
pub mod highlight_tone_priority;
pub mod number_visitor;

use std::path::Path;
use std::process::Command;

use chrono::DateTime;
use chrono::FixedOffset;
use date_time::deserialize_date_time;
use date_time::deserialize_optional_date_time;
use highlight_tone_priority::deserialize_highlight_tone_priority;
use highlight_tone_priority::HighlightTonePriority;
use serde::Deserialize;
use serde::Serialize;
use serde::{self,};

use self::number_visitor::deserialize_number;
use self::number_visitor::deserialize_optional_number;
use crate::error::RawHelperError;
use crate::error::RawHelperResult;
use crate::number::Number;

#[derive(Clone, Serialize, Deserialize)]
pub struct Exiftool
{
    #[serde(rename = "EXIF")]
    pub exif: Option<Exif>,

    #[serde(rename = "MakerNotes")]
    pub maker_notes: Option<MakerNotes>,

    #[serde(rename = "Composite")]
    pub composite: Composite,

    #[serde(rename = "QuickTime")]
    pub quick_time: Option<QuickTime>,

    #[serde(rename = "XMP")]
    pub xmp: Option<Xmp>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Exif
{
    #[serde(rename = "Make")]
    pub make: String,

    #[serde(rename = "Model")]
    pub model: String,

    #[serde(rename = "ISO")]
    pub iso: i64,

    #[serde(rename = "DateTimeOriginal", deserialize_with = "deserialize_date_time")]
    pub date_time_original: DateTime<FixedOffset>,

    #[serde(rename = "ApertureValue")]
    pub aperture_value: Option<f64>,

    #[serde(
        rename = "ExposureCompensation",
        deserialize_with = "deserialize_optional_number",
        default
    )]
    pub exposure_compensation: Option<Number>,

    #[serde(rename = "Flash")]
    pub flash: Option<String>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct MakerNotes
{
    #[serde(rename = "AutoISO")]
    pub auto_iso: i64,

    #[serde(rename = "BaseISO")]
    pub base_iso: Option<i64>,

    #[serde(rename = "ExposureCompensation", deserialize_with = "deserialize_number")]
    pub exposure_compensation: Number,

    #[serde(rename = "AEBBracketValue", deserialize_with = "deserialize_number")]
    pub aeb_bracket_value: Number,

    #[serde(rename = "ExposureTime", deserialize_with = "deserialize_optional_number", default)]
    pub exposure_time: Option<Number>,

    #[serde(rename = "BulbDuration")]
    pub bulb_duration: f64,

    #[serde(
        rename = "HighlightTonePriority",
        deserialize_with = "deserialize_highlight_tone_priority"
    )]
    pub highlight_tone_priority: HighlightTonePriority,

    #[serde(rename = "TimeStamp", deserialize_with = "deserialize_optional_date_time", default)]
    pub time_stamp: Option<DateTime<FixedOffset>>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Composite
{
    #[serde(rename = "ISO")]
    pub iso: Option<i64>,

    #[serde(rename = "Aperture", deserialize_with = "deserialize_optional_number", default)]
    pub aperture: Option<Number>,

    #[serde(rename = "ShutterSpeed", deserialize_with = "deserialize_optional_number", default)]
    pub shutter_speed: Option<Number>,

    #[serde(
        rename = "SubSecCreateDate",
        deserialize_with = "deserialize_optional_date_time",
        default
    )]
    pub sub_sec_create_date: Option<DateTime<FixedOffset>>,

    #[serde(
        rename = "SubSecDateTimeOriginal",
        deserialize_with = "deserialize_optional_date_time",
        default
    )]
    pub sub_sec_date_time_original: Option<DateTime<FixedOffset>>,

    #[serde(
        rename = "SubSecModifyDate",
        deserialize_with = "deserialize_optional_date_time",
        default
    )]
    pub sub_sec_modify_date: Option<DateTime<FixedOffset>>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct QuickTime
{
    #[serde(rename = "CreateDate", deserialize_with = "deserialize_date_time")]
    pub create_date: DateTime<FixedOffset>,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Xmp
{
    #[serde(
        rename = "DateTimeOriginal",
        deserialize_with = "deserialize_optional_date_time",
        default
    )]
    pub date_time_original: Option<DateTime<FixedOffset>>,
}

impl TryFrom<Vec<u8>> for Exiftool
{
    type Error = RawHelperError;

    fn try_from(bytes: Vec<u8>) -> RawHelperResult<Exiftool>
    {
        let model = serde_json::from_slice::<Vec<Exiftool>>(&bytes)?;

        if model.len() != 1
        {
            return Err(RawHelperError::ExiftoolNonOneItems { count: model.len() });
        }

        let model = model
            .into_iter()
            .next()
            .ok_or(RawHelperError::ExiftoolNonOneItems { count: 0 })?;

        Ok(model)
    }
}

impl Exiftool
{
    pub(crate) fn find_date_time(&self) -> Option<DateTime<FixedOffset>>
    {
        match self
        {
            // with subsecond
            Exiftool {
                composite:
                    Composite {
                        sub_sec_date_time_original: Some(sub_sec_date_time_original),
                        ..
                    },
                ..
            } => Some(*sub_sec_date_time_original),
            Exiftool {
                composite:
                    Composite {
                        sub_sec_create_date: Some(sub_sec_create_date),
                        ..
                    },
                ..
            } => Some(*sub_sec_create_date),
            Exiftool {
                composite:
                    Composite {
                        sub_sec_modify_date: Some(sub_sec_modify_date),
                        ..
                    },
                ..
            } => Some(*sub_sec_modify_date),
            Exiftool {
                maker_notes:
                    Some(MakerNotes {
                        time_stamp: Some(time_stamp),
                        ..
                    }),
                ..
            } => Some(*time_stamp),

            // without subsecond, with timezone
            Exiftool {
                quick_time: Some(QuickTime { create_date, .. }),
                ..
            } => Some(*create_date),

            // without subsecond, without timezone, but at least actual shooting start timestamp
            Exiftool {
                exif: Some(Exif { date_time_original, .. }),
                ..
            } => Some(*date_time_original),

            // not ideal... seems to be something close to actual shooting start timestamp, but not the same
            Exiftool {
                xmp:
                    Some(Xmp {
                        date_time_original: Some(date_time_original),
                        ..
                    }),
                ..
            } => Some(*date_time_original),

            _ => None,
        }
    }

    pub(crate) fn find_iso(&self) -> Option<f64>
    {
        match self
        {
            Exiftool {
                maker_notes:
                    Some(MakerNotes {
                        base_iso: Some(base_iso),
                        auto_iso,
                        ..
                    }),
                ..
            } => Some((base_iso * auto_iso) as f64 / 100.0),
            Exiftool {
                composite: Composite { iso: Some(iso), .. },
                ..
            } => Some(*iso as f64),
            Exiftool { exif: Some(exif), .. } => Some(exif.iso as f64),
            _ => None,
        }
    }

    pub(crate) fn find_aperture(&self) -> Option<f64>
    {
        match self
        {
            Exiftool {
                exif:
                    Some(Exif {
                        aperture_value: Some(aperture_value),
                        ..
                    }),
                ..
            } if *aperture_value > 0.0 => Some(*aperture_value),
            Exiftool {
                composite:
                    Composite {
                        aperture: Some(aperture),
                        ..
                    },
                ..
            } if *aperture > Number::Zero => Some(f64::from(aperture)),
            _ => None,
        }
    }

    pub(crate) fn find_highlight_tone_priority(&self) -> HighlightTonePriority
    {
        match self.maker_notes.as_ref()
        {
            Some(MakerNotes {
                highlight_tone_priority,
                ..
            }) => highlight_tone_priority.clone(),
            None => HighlightTonePriority::Off,
        }
    }

    pub(crate) fn find_shutter_speed(&self) -> Option<f64>
    {
        match self
        {
            Exiftool {
                maker_notes: Some(MakerNotes { bulb_duration, .. }),
                ..
            } if *bulb_duration > 0.0 => Some(*bulb_duration),
            Exiftool {
                maker_notes:
                    Some(MakerNotes {
                        exposure_time: Some(exposure_time),
                        ..
                    }),
                ..
            } if *exposure_time != Number::Zero => Some(f64::from(exposure_time)),
            Exiftool {
                composite:
                    Composite {
                        shutter_speed: Some(shutter_speed),
                        ..
                    },
                ..
            } if *shutter_speed != Number::Zero => Some(f64::from(shutter_speed)),
            _ => None,
        }
    }

    pub(crate) fn find_exposure_compensation(&self) -> Option<f64>
    {
        match self
        {
            Exiftool {
                maker_notes: Some(MakerNotes {
                    exposure_compensation, ..
                }),
                ..
            } if *exposure_compensation != Number::Zero => Some(f64::from(exposure_compensation)),
            Exiftool {
                exif:
                    Some(Exif {
                        exposure_compensation: Some(exposure_compensation),
                        ..
                    }),
                ..
            } if *exposure_compensation != Number::Zero => Some(f64::from(exposure_compensation)),
            _ => None,
        }
    }

    pub(crate) fn find_aeb_compensation(&self) -> Option<f64>
    {
        match self.maker_notes.as_ref()
        {
            Some(MakerNotes { aeb_bracket_value, .. }) => Some(f64::from(aeb_bracket_value)),
            None => None,
        }
    }

    pub(crate) fn find_make_and_model(&self) -> Option<String>
    {
        match &self.exif
        {
            Some(Exif { make, model, .. }) => Some(format!("{} - {}", make, model)),
            _ => None,
        }
    }

    pub(crate) fn find_flash_fired(&self) -> bool
    {
        match &self.exif
        {
            Some(Exif { flash: Some(flash), .. }) => flash.starts_with("On"),
            _ => false,
        }
    }

    pub(crate) fn load_exif_tags(file_path: &Path) -> RawHelperResult<Exiftool>
    {
        let output = Command::new("exiftool")
            .arg("-struct")
            .arg("-groupHeadings")
            .arg("-api")
            .arg("QuickTimeUTC")
            .arg("-json")
            .arg(file_path)
            .output()?;

        if output.status.success()
        {
            // println!("{}", String::from_utf8_lossy(&output.stdout).to_string());
            let exiftool_output: RawHelperResult<Exiftool> = output.stdout.try_into();
            exiftool_output
        }
        else
        {
            Err(RawHelperError::ExiftoolRunFailure {
                stderr: String::from_utf8_lossy(&output.stderr).to_string(),
                code:   output.status.code().expect("exiftool assumed to have exited already"),
            })
        }
    }
}
