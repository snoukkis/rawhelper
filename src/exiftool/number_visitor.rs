use std::fmt;

use serde::de;

use crate::number::Number;
use crate::number::NumberConversionError;

pub(crate) fn deserialize_optional_number<'de, D>(d: D) -> Result<Option<Number>, D::Error>
where D: de::Deserializer<'de>
{
    d.deserialize_any(OptionalNumberVisitor)
}

struct OptionalNumberVisitor;

impl<'de> de::Visitor<'de> for OptionalNumberVisitor
{
    type Value = Option<Number>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result
    {
        write!(formatter, "exiftool number or string representing fractional or null")
    }

    fn visit_none<E>(self) -> Result<Self::Value, E>
    where E: de::Error
    {
        Ok(None)
    }

    fn visit_some<D>(self, d: D) -> Result<Option<Number>, D::Error>
    where D: de::Deserializer<'de>
    {
        Ok(Some(deserialize_number(d)?))
    }

    fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
    where E: de::Error
    {
        NumberVisitor.visit_u64::<E>(v).map(|n| Some(n))
    }

    fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
    where E: de::Error
    {
        NumberVisitor.visit_i64::<E>(v).map(|n| Some(n))
    }

    fn visit_f64<E>(self, v: f64) -> Result<Self::Value, E>
    where E: de::Error
    {
        NumberVisitor.visit_f64::<E>(v).map(|n| Some(n))
    }

    fn visit_str<E>(self, fraction: &str) -> Result<Self::Value, E>
    where E: de::Error
    {
        match fraction
        {
            "undef" => Ok(None),
            _ => NumberVisitor.visit_str::<E>(fraction).map(|n| Some(n)),
        }
    }
}

struct NumberVisitor;

pub(crate) fn deserialize_number<'de, D>(d: D) -> Result<Number, D::Error>
where D: de::Deserializer<'de>
{
    d.deserialize_any(NumberVisitor)
}

impl<'de> de::Visitor<'de> for NumberVisitor
{
    type Value = Number;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result
    {
        write!(formatter, "exiftool number or string representing fractional")
    }

    fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
    where E: de::Error
    {
        Ok(Number::Uint(v))
    }

    fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
    where E: de::Error
    {
        Ok(Number::Int(v))
    }

    fn visit_f64<E>(self, v: f64) -> Result<Self::Value, E>
    where E: de::Error
    {
        Ok(Number::Float(v))
    }

    fn visit_str<E>(self, fraction: &str) -> Result<Self::Value, E>
    where E: de::Error
    {
        let conversion_result = Self::Value::try_from(fraction);

        conversion_result.map_err(|raw_helper_error| match raw_helper_error
        {
            NumberConversionError::ParseIntError { wrapped } =>
            {
                E::custom(format!("Can't parse fraction ({}) parts as int: {}", fraction, wrapped))
            },
            NumberConversionError::FractionWithBadPartsCount { count } =>
            {
                E::custom(format!("Fraction ({}) has more than two parts ({})", fraction, count))
            },
        })
    }
}
