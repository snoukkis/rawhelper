use std::num::ParseIntError;

use serde::Deserialize;
use serde::Serialize;

#[derive(Clone, Serialize, Deserialize)]
pub enum Number
{
    Zero,
    Float(f64),
    Int(i64),
    Uint(u64),
    Fraction
    {
        numerator:   i64,
        denominator: i64,
    },
}

impl From<&Number> for f64
{
    fn from(number: &Number) -> Self
    {
        match number
        {
            Number::Zero => 0.0,
            Number::Float(number) => *number,
            Number::Int(number) => *number as f64,
            Number::Uint(number) => *number as f64,
            Number::Fraction { numerator, denominator } => *numerator as f64 / *denominator as f64,
        }
    }
}

impl PartialEq for Number
{
    fn eq(&self, other: &Self) -> bool { f64::from(self) == f64::from(other) }
}

impl Eq for Number {}

impl PartialOrd for Number
{
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering>
    {
        match (f64::from(self), f64::from(other))
        {
            (s, o) if s < o => Some(std::cmp::Ordering::Less),
            (s, o) if s == o => Some(std::cmp::Ordering::Equal),
            (s, o) if s > o => Some(std::cmp::Ordering::Greater),

            _ => None,
        }
    }
}

impl Ord for Number
{
    fn cmp(&self, other: &Self) -> std::cmp::Ordering { self.partial_cmp(other).expect("impossible") }
}

#[rustfmt::skip]
pub enum NumberConversionError
{
    ParseIntError { wrapped: ParseIntError },
    FractionWithBadPartsCount { count: usize },
}

impl TryFrom<&str> for Number
{
    type Error = NumberConversionError;

    fn try_from(fraction: &str) -> Result<Self, Self::Error>
    {
        let parts: Vec<&str> = fraction.split("/").collect();
        let mut parts = parts.into_iter();

        match parts.len()
        {
            1 => Ok(Number::Int(
                parts
                    .next()
                    .expect("we just checked the count")
                    .parse::<i64>()
                    .map_err(|parse_int_error| NumberConversionError::ParseIntError {
                        wrapped: parse_int_error,
                    })?,
            )),
            2 => Ok(Number::Fraction {
                numerator:   parts
                    .next()
                    .expect("we just checked the count")
                    .parse::<i64>()
                    .map_err(|parse_int_error| NumberConversionError::ParseIntError {
                        wrapped: parse_int_error,
                    })?,
                denominator: parts
                    .next()
                    .expect("we just checked the count")
                    .parse::<i64>()
                    .map_err(|parse_int_error| NumberConversionError::ParseIntError {
                        wrapped: parse_int_error,
                    })?,
            }),
            count => Err(NumberConversionError::FractionWithBadPartsCount { count }),
        }
    }
}
