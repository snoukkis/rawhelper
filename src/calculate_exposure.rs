use std::path::Path;

use crate::error::RawHelperError;
use crate::error::RawHelperResult;
use crate::number::Number;
use crate::spec;
use crate::spec::Spec;

pub(crate) fn calculate_exposure(
    input_spec_path: &Path,
    output_spec_path: &Path,
    compensation: Option<String>,
) -> RawHelperResult<()>
{
    let mut spec = Spec::load_from_file(input_spec_path)?;

    let compensation = match compensation
    {
        Some(compensation) => f64::from(&Number::try_from(compensation.as_str())?),
        None => 0.0,
    };

    let groups_and_images = spec::pair_groups_with_images(&mut spec.groups, &spec.images)?;

    // For each group: target_ev = median + compensation.
    groups_and_images.into_iter().try_for_each(|(_, group_and_images)| {
        let measured_ev_values = group_and_images
            .images
            .iter()
            .map(|image| {
                image.measured_ev.ok_or(RawHelperError::ImageMissingMeasuredEV {
                    path: image.path.to_string(),
                })
            })
            .collect::<Result<Vec<f64>, RawHelperError>>()?;

        if let Some(measured_ev_median) = stats::median(measured_ev_values.into_iter())
        {
            let target_ev = measured_ev_median + compensation;
            group_and_images.group.target_ev = target_ev;
        }

        RawHelperResult::Ok(())
    })?;

    spec.save_to_file(output_spec_path)?;

    Ok(())
}
