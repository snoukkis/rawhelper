use std::ffi::OsStr;
use std::path::Path;

pub trait PathExtension
{
    fn file_prefix(&self) -> Option<&OsStr>;
    fn multi_extension(&self) -> Option<&OsStr>;
}

impl PathExtension for Path
{
    fn file_prefix(&self) -> Option<&OsStr>
    {
        self.file_name()
            .map(split_file_at_dot)
            .and_then(|(before, _)| Some(before))
    }

    fn multi_extension(&self) -> Option<&OsStr>
    {
        self.file_name().map(split_file_at_dot).and_then(|(_, after)| after)
    }
}

fn split_file_at_dot(file: &OsStr) -> (&OsStr, Option<&OsStr>)
{
    let slice = os_str_as_u8_slice(file);
    if slice == b".."
    {
        return (file, None);
    }

    // The unsafety here stems from converting between &OsStr and &[u8]
    // and back. This is safe to do because (1) we only look at ASCII
    // contents of the encoding and (2) new &OsStr values are produced
    // only from ASCII-bounded slices of existing &OsStr values.
    let i = match slice[1..].iter().position(|b| *b == b'.')
    {
        Some(i) => i + 1,
        None => return (file, None),
    };
    let before = &slice[..i];
    let after = &slice[i + 1..];
    unsafe { (u8_slice_as_os_str(before), Some(u8_slice_as_os_str(after))) }
}

fn os_str_as_u8_slice(s: &OsStr) -> &[u8] { unsafe { &*(s as *const OsStr as *const [u8]) } }

#[allow(unused_unsafe)]
unsafe fn u8_slice_as_os_str(s: &[u8]) -> &OsStr
{
    // SAFETY: see the comment of `os_str_as_u8_slice`
    unsafe { &*(s as *const [u8] as *const OsStr) }
}
