use std::path::PathBuf;

use clap::AppSettings;
use clap::Subcommand;

#[derive(Subcommand)]
pub enum Commands
{
    /// Create spec file.
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Init
    {
        /// Where to write the spec file to.
        #[clap(long, short)]
        output_spec: PathBuf,

        /// Directory with media files.
        #[clap(long, short)]
        media_directory: PathBuf,

        /// Extensions of media files.
        #[clap(long, short)]
        extensions: Vec<String>,

        /// Skip exposure calculation. Useful for e.g. renaming videos.
        #[clap(long)]
        date_only: bool,

        /// Pixel width in μm.
        #[clap(long)]
        pixel_width: Option<f64>,
    },

    /// Sometimes companion jpeg files have better timestamp than raw files. E.g. on OnePlus 6.
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    RereadTimestamp
    {
        /// Where to read the spec files from.
        #[clap(long, short)]
        input_spec: PathBuf,

        /// Where to write the spec file to.
        #[clap(long, short)]
        output_spec: PathBuf,

        /// Directory with media files.
        #[clap(long, short)]
        media_directory: PathBuf,

        /// Extensions of media files.
        #[clap(long, short)]
        extensions: Vec<String>,
    },

    /// Rename files.
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Rename
    {
        /// Where to read the spec file from.
        #[clap(long, short)]
        input_spec: PathBuf,

        /// Where to write the spec file to.
        #[clap(long, short)]
        output_spec: PathBuf,

        /// Directories with media files.
        #[clap(long, short)]
        media_directories: Vec<PathBuf>,
    },

    /// Guess groups based on timestamp and exporsure.
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    GuessGroups
    {
        /// Where to read the spec file from.
        #[clap(long, short)]
        input_spec: PathBuf,

        /// Where to write the spec file to.
        #[clap(long, short)]
        output_spec: PathBuf,

        /// Extensions of media files.
        #[clap(long, short)]
        extensions: Vec<String>,

        /// Directory with profiles.
        #[clap(long, short)]
        profile_directory: PathBuf,
    },

    /// Set all group exposures to median.
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    CalculateExposure
    {
        /// Where to read the spec file from.
        #[clap(long, short)]
        input_spec: PathBuf,

        /// Where to write the spec file to.
        #[clap(long, short)]
        output_spec: PathBuf,

        /// Exposure compensation. Use double quoting to pass negative values. Shell issue...
        #[clap(long, allow_hyphen_values = true)]
        compensation: Option<String>,
    },

    /// Write a spec file to profiles.
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    WriteProfiles
    {
        /// Where to read the spec file from.
        #[clap(long, short)]
        input_spec: PathBuf,

        /// Extensions of media files.
        #[clap(long, short)]
        extensions: Vec<String>,

        /// Directory with profiles.
        #[clap(long, short)]
        profile_directory: PathBuf,
    },

    /// Convert to jpeg.
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Jpeg
    {
        /// Where to read the spec file from.
        #[clap(long, short)]
        input_spec: PathBuf,

        /// Directory with media files.
        #[clap(short, long, short)]
        media_directory: PathBuf,

        /// Extensions of media files.
        #[clap(long, short)]
        extensions: Vec<String>,

        /// Directory where to write jpeg files.
        #[clap(long)]
        jpeg_directory: PathBuf,

        /// base bytes per pixel (as  EV)
        #[clap(long)]
        base_bytes_per_pixel: Option<f64>,

        /// path to icc profile
        #[clap(long)]
        icc: Option<PathBuf>,

        /// orientation agnostic
        #[clap(long)]
        orientation_agnostic: bool,

        /// max resolution
        #[clap(long)]
        max_resolution: Option<String>,
    },

    /// Fix timestamps.
    #[clap(setting(AppSettings::ArgRequiredElseHelp))]
    Datefix
    {
        /// Where to read the spec file from.
        #[clap(long, short)]
        input_spec: PathBuf,

        /// Directories with media files.
        #[clap(long, short)]
        media_directories: Vec<PathBuf>,
    },
}
