mod calculate_exposure;
mod command;
mod date_path;
mod error;
mod exiftool;
mod filesystem;
mod group;
mod init;
mod keyfile;
mod number;
mod path_extension;
mod rename;
mod spec;
mod write_profiles;

extern crate chrono;
extern crate chrono_tz;

use clap::Parser;
use command::Commands;
use error::RawHelperResult;

#[derive(Parser)]
#[clap(name = "rawhelper")]
#[clap(about = "Equalize exposure, rename, fix date, etc.")]
struct Cli
{
    #[clap(subcommand)]
    command: Commands,
}

fn main() -> RawHelperResult<()>
{
    let args = Cli::parse();

    #[allow(unused_variables)]
    match args.command
    {
        Commands::Init {
            output_spec,
            media_directory,
            extensions,
            date_only,
            pixel_width,
        } => init::init(&output_spec, &media_directory, extensions, date_only, pixel_width),

        Commands::RereadTimestamp {
            input_spec,
            output_spec,
            media_directory,
            extensions,
        } => init::reread_timestamp(&input_spec, &output_spec, &media_directory, extensions),

        Commands::Rename {
            input_spec,
            output_spec,
            media_directories,
        } => rename::rename(&input_spec, &output_spec, media_directories),

        Commands::GuessGroups {
            input_spec,
            output_spec,
            extensions,
            profile_directory,
        } => group::guess_groups(&input_spec, &output_spec, extensions, &profile_directory),

        Commands::CalculateExposure {
            input_spec,
            output_spec,
            compensation,
        } => calculate_exposure::calculate_exposure(&input_spec, &output_spec, compensation),

        Commands::WriteProfiles {
            input_spec,
            extensions,
            profile_directory,
        } => write_profiles::write_profiles(&input_spec, extensions, &profile_directory),

        Commands::Jpeg {
            input_spec,
            media_directory,
            extensions,
            jpeg_directory,
            base_bytes_per_pixel,
            icc,
            orientation_agnostic,
            max_resolution,
        } => todo!(),

        Commands::Datefix {
            input_spec,
            media_directories,
        } => todo!(),
    }
}
