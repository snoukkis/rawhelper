use std::path::Path;
use std::path::PathBuf;

use crate::error::RawHelperError;
use crate::error::RawHelperResult;
use crate::path_extension::PathExtension;


pub(crate) fn disambiguate_path(
    image_file_path_without_extension: &Path,
    accepted_extensions: &Vec<String>,
) -> RawHelperResult<PathBuf>
{
    accepted_extensions
        .iter()
        .find_map(|extension_candidate| {
            let mut path = image_file_path_without_extension.to_owned();
            path.set_extension(extension_candidate);

            match path.metadata()
            {
                Ok(metadata) =>
                {
                    if metadata.is_file()
                    {
                        Some(Ok(path))
                    }
                    else
                    {
                        Some(Err(RawHelperError::MediaFileNotFile { path }))
                    }
                },
                Err(err) => match err.kind()
                {
                    std::io::ErrorKind::NotFound => None,
                    _ => Some(Err(RawHelperError::from(err))),
                },
            }
        })
        .transpose()?
        .ok_or(RawHelperError::MediaFileMissing {
            path: image_file_path_without_extension.to_owned(),
        })
}

pub(crate) fn has_accepted_extension(path: &Path, accepted_extensions: &Vec<String>) -> bool
{
    let extension = path.multi_extension().and_then(|e| e.to_str());
    let extension = match extension
    {
        Some(e) => e,
        None => return false,
    }
    .to_lowercase();

    accepted_extensions
        .iter()
        .any(|accepted_extension| extension == accepted_extension.to_lowercase())
}

pub(crate) fn resolve_profile_path(image_path: &str, image_extensions: &Vec<String>, profile_directory: &Path) -> RawHelperResult<PathBuf>
{
    let mut path = profile_directory.to_owned();
    path.push(image_path);
    let extensions = image_extensions
        .iter()
        .map(|ext| format!("{}.pp3", ext))
        .collect::<Vec<String>>();

    disambiguate_path(&path, &extensions)
}
