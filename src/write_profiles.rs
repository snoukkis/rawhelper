use std::path::Path;

use crate::error::RawHelperError;
use crate::error::RawHelperResult;
use crate::filesystem;
use crate::keyfile;
use crate::spec;
use crate::spec::Group;
use crate::spec::GroupAndImages;
use crate::spec::Image;
use crate::spec::Spec;


pub(crate) fn write_profiles(
    input_spec_path: &Path,
    image_extensions: Vec<String>,
    profile_directory: &Path,
) -> RawHelperResult<()>
{
    let mut spec = Spec::load_from_file(input_spec_path)?;

    let groups_and_images = spec::pair_groups_with_images(&mut spec.groups, &spec.images)?;

    groups_and_images
        .iter()
        .enumerate()
        .try_for_each(|(index, (_, GroupAndImages { group, images }))| {
            process(group, images, index, &image_extensions, profile_directory)
        })?;

    Ok(())
}

fn process(group: &Group, images: &Vec<&Image>, index: usize, image_extensions: &Vec<String>, profile_directory: &Path) -> RawHelperResult<()>
{
    images.iter().try_for_each(|image| {
        let exposed_ev = image.exposed_ev.ok_or(RawHelperError::ImageMissingExposedEV { path: image.path.to_string() })?;
        let compensation = exposed_ev - group.target_ev;

        let profile_path = filesystem::resolve_profile_path(&image.path, &image_extensions, profile_directory)?;

        let profile = keyfile::Profile::load(&profile_path)?;

        profile.set_exposure_compensation(compensation);
        profile.set_exposure_highlight_compr_threshold(0);

        if compensation < 0.0 {
            // overexposed pixels need to be guessed when using negative compensation, use the magic Color method by default
            profile.set_hl_recovery_enabled(true);
            profile.set_hl_recovery_method("Color");

            // no point in compressing guessed highlights
            profile.set_exposure_highlight_compr(0);
        } else {
            // no need to guess lost highlights in this case
            profile.set_hl_recovery_enabled(false);

            // extra highlights are available, let's compress them
            profile.set_exposure_highlight_compr(75);
        }

        let color_label = u64::try_from(index % 5 + 1)?;
        profile.set_color_label(color_label);

        profile.save()?;

        RawHelperResult::Ok(())
    })
}
