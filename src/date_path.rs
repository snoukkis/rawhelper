use chrono::DateTime;
use chrono::FixedOffset;

const FORMAT: &str = "%Y-%m-%dT%H-%M-%S-%f";

pub(crate) fn path_format(timestamp: DateTime<FixedOffset>) -> String
{
    timestamp
        .format(FORMAT)
        .to_string()
}
