use std::path::Path;

use chrono::Duration;
use linked_hash_map::LinkedHashMap;

use crate::date_path;
use crate::error::RawHelperError;
use crate::error::RawHelperResult;
use crate::filesystem;
use crate::keyfile;
use crate::spec::Group;
use crate::spec::Image;
use crate::spec::Spec;

type Grouping<'a> = Vec<&'a mut Image>;

struct GroupBuilder<'a>
{
    groups: Vec<Grouping<'a>>,
    group:  Grouping<'a>,
}

impl<'a> GroupBuilder<'a>
{
    fn new() -> Self
    {
        GroupBuilder {
            groups: vec![],
            group:  vec![],
        }
    }

    fn add_to_group(&mut self, image: &'a mut Image) { self.group.push(image); }

    fn current_group(&self) -> &Grouping<'a> { &self.group }

    fn group_length(&self) -> usize { self.group.len() }

    fn split(&mut self)
    {
        let mut done: Grouping = vec![];
        done.extend(self.group.drain(..));
        self.groups.push(done);
    }

    fn finish(mut self) -> Vec<Grouping<'a>>
    {
        self.groups.push(self.group);

        self.groups
    }
}

fn auto_group_images(images: &mut Vec<Image>) -> RawHelperResult<Vec<Grouping>>
{
    if images.is_empty()
    {
        return Ok(vec![]);
    }

    type BoundryChecker = fn(&Grouping, &Image) -> RawHelperResult<bool>;

    let is_time_boundry: BoundryChecker = |group, next| {
        let ref previous_image_timestamp = group.last().unwrap().timestamp;
        let ref current_image_timestamp = next.timestamp;
        let time_diff = *current_image_timestamp - *previous_image_timestamp;

        Ok(time_diff > Duration::minutes(15))
    };

    let is_flash_boundry: BoundryChecker = |group, next| Ok(group.last().unwrap().flash != next.flash);

    let is_exposure_boundry: BoundryChecker = |group, next| {
        let exposure_diff_limit = 5.0 / 3.0; // magic number from from statistics

        let measured_ev_list = group
            .iter()
            .map(|image| -> Result<f64, RawHelperError> {
                image.measured_ev.ok_or(RawHelperError::ImageMissingMeasuredEV {
                    path: image.path.to_string(),
                })
            })
            .collect::<Result<Vec<f64>, RawHelperError>>()?;

        let measured_ev_median =
            stats::median(measured_ev_list.into_iter()).expect("Grouping should have enough items for median.");
        let next_measured_ev = next.measured_ev.ok_or(RawHelperError::ImageMissingMeasuredEV {
            path: next.path.to_string(),
        })?;

        let diff_from_median = (next_measured_ev - measured_ev_median).abs();

        Ok(diff_from_median > exposure_diff_limit)
    };

    let boundry_checks: Vec<BoundryChecker> = vec![is_time_boundry, is_flash_boundry, is_exposure_boundry];

    let mut group_builder = GroupBuilder::new();

    images.iter_mut().try_for_each(|next| {
        if group_builder.group_length() > 0
        {
            let found_boundry = boundry_checks
                .iter()
                .find_map(|boundry_check| match boundry_check(group_builder.current_group(), next)
                {
                    Ok(true) => Some(Ok(())),
                    Ok(false) => None,
                    Err(error) => Some(Err(error)),
                })
                .transpose()?;

            if let Some(_) = found_boundry
            {
                group_builder.split();
            }
        }

        group_builder.add_to_group(next);

        RawHelperResult::Ok(())
    })?;

    Ok(group_builder.finish())
}

pub(crate) fn guess_groups(
    input_spec_path: &Path,
    output_spec_path: &Path,
    image_extensions: Vec<String>,
    profile_directory: &Path,
) -> RawHelperResult<()>
{
    let mut spec = Spec::load_from_file(input_spec_path)?;

    let mut auto_grouped = auto_group_images(&mut spec.images)?;

    let mut groups = LinkedHashMap::<String, Group>::new();
    auto_grouped.iter_mut().enumerate().try_for_each(|(index, images)| {
        let first_image = images.first().expect("Each grouping should have at least one image.");
        let group_name = format!("{}-unnamed", date_path::path_format(first_image.timestamp));

        images.iter_mut().try_for_each(|image| {
            image.group = group_name.clone();

            let profile_path = filesystem::resolve_profile_path(&image.path, &image_extensions, profile_directory)?;

            let profile = keyfile::Profile::load(&profile_path)?;

            let color_label = (index % 5 + 1).try_into().expect("this will be ok");
            profile.set_color_label(color_label);

            profile.save()?;

            RawHelperResult::Ok(())
        })?;

        groups.insert(group_name, Group { target_ev: 0.0 });

        RawHelperResult::Ok(())
    })?;

    spec.groups = groups;

    spec.save_to_file(&output_spec_path)?;

    Ok(())
}
