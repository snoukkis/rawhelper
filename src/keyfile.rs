#![allow(dead_code)]

use std::path::Path;
use std::path::PathBuf;

use glib::KeyFile;
use glib::KeyFileFlags;

use crate::error::RawHelperResult;

pub struct Profile
{
    path: PathBuf,
    key_file: KeyFile,
}

impl Profile
{
    pub(crate) fn load(path: &Path) -> RawHelperResult<Self>
    {
        Ok(Profile { path: path.to_owned(), key_file: Self::load_key_file(path)? })
    }

    pub(crate) fn save(self) -> RawHelperResult<()>
    {
        self.key_file.save_to_file(self.path)?;

        Ok(())
    }

    fn load_key_file(path: &Path) -> RawHelperResult<KeyFile>
    {
        let key_file = KeyFile::new();
        key_file.load_from_file(
            path,
            KeyFileFlags::NONE | KeyFileFlags::KEEP_COMMENTS | KeyFileFlags::KEEP_TRANSLATIONS,
        )?;

        Ok(key_file)
    }

    pub(crate) fn set_color_label(&self, color_label: u64)
    {
        self.key_file.set_uint64("General", "ColorLabel", color_label);
    }

    pub(crate) fn set_exposure_compensation(&self, compensation: f64)
    {
        self.key_file.set_double("Exposure", "Compensation", compensation);
    }

    pub(crate) fn set_exposure_highlight_compr_threshold(&self, highlight_compr_threshold: u64)
    {
        self.key_file.set_uint64("Exposure", "HighlightComprThreshold", highlight_compr_threshold);
    }

    pub(crate) fn set_exposure_highlight_compr(&self, highlight_compr: u64)
    {
        self.key_file.set_uint64("Exposure", "HighlightCompr", highlight_compr);
    }

    pub(crate) fn set_hl_recovery_enabled(&self, enabled: bool)
    {
        self.key_file.set_boolean("HLRecovery", "Enabled", enabled);
    }

    pub(crate) fn set_hl_recovery_method(&self, method: &str)
    {
        self.key_file.set_string("HLRecovery", "Method", method);
    }
}
