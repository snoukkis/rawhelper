use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::Path;

use chrono::DateTime;
use chrono::FixedOffset;
use linked_hash_map::Entry::Occupied;
use linked_hash_map::Entry::Vacant;
use linked_hash_map::LinkedHashMap;
use serde::Deserialize;
use serde::Serialize;
use serde_json::Error as SerdeJsonError;

use crate::error::RawHelperError;
use crate::error::RawHelperResult;

#[derive(Serialize, Deserialize)]
pub struct Group
{
    #[serde(rename = "targetEV")]
    pub target_ev: f64,
}

#[derive(Serialize, Deserialize)]
pub struct Image
{
    pub path: String,

    pub group: String,

    pub timestamp: DateTime<FixedOffset>,

    #[serde(rename = "exposedEV", skip_serializing_if = "Option::is_none")]
    pub exposed_ev: Option<f64>,

    #[serde(rename = "measuredEV", skip_serializing_if = "Option::is_none")]
    pub measured_ev: Option<f64>,

    #[serde(rename = "gainEV", skip_serializing_if = "Option::is_none")]
    pub gain_ev: Option<f64>,

    #[serde(rename = "pixelAreaEV", skip_serializing_if = "Option::is_none")]
    pub pixel_area_ev: Option<f64>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub flash: Option<bool>,
}

impl Image
{
    pub(crate) fn new_date_only(path: String, timestamp: DateTime<FixedOffset>) -> Image
    {
        Image {
            path,
            timestamp,
            exposed_ev: None,
            measured_ev: None,
            gain_ev: None,
            pixel_area_ev: None,
            flash: None,
            group: "".to_string(),
        }
    }
}

type GroupsMap = LinkedHashMap<String, Group>;

#[derive(Serialize, Deserialize)]
pub struct Spec
{
    pub groups: GroupsMap,
    pub images: Vec<Image>,
}

impl TryFrom<&str> for Spec
{
    type Error = SerdeJsonError;

    fn try_from(json_string: &str) -> Result<Spec, SerdeJsonError>
    {
        let model = serde_json::from_str(json_string)?;
        Ok(model)
    }
}

impl TryInto<String> for &Spec
{
    type Error = SerdeJsonError;

    fn try_into(self) -> Result<String, SerdeJsonError>
    {
        let json_string = serde_json::to_string_pretty(&self)?;
        Ok(json_string)
    }
}

impl Spec
{
    pub(crate) fn new() -> Self
    {
        Self {
            groups: LinkedHashMap::new(),
            images: Vec::new(),
        }
    }

    pub(crate) fn load_from_file(path: &Path) -> Result<Self, std::io::Error>
    {
        let mut file = File::open(path)?;

        let mut json_string = String::new();
        file.read_to_string(&mut json_string)?;
        // println!("json_string:\n{}", json_string);

        let model = <Self as TryFrom<&str>>::try_from(&json_string)?;

        Ok(model)
    }

    pub(crate) fn save_to_file(&self, path: &Path) -> Result<(), std::io::Error>
    {
        let json_string: String = self.try_into()?;
        // println!("{}", json_string);

        let mut file = File::create(&path)?;

        file.write_all(json_string.as_bytes())?;

        Ok(())
    }
}

pub type GroupsAndImagesMap<'a> = LinkedHashMap<String, GroupAndImages<'a>>;

pub struct GroupAndImages<'a>
{
    pub group:  &'a mut Group,
    pub images: Vec<&'a Image>,
}

pub fn pair_groups_with_images<'a>(
    groups: &'a mut GroupsMap,
    images: &'a Vec<Image>,
) -> RawHelperResult<GroupsAndImagesMap<'a>>
{
    let groups_and_images = pair_groups_with_empty_image_vec(groups);

    match_images_with_groups(groups_and_images, images)
}

fn pair_groups_with_empty_image_vec<'a>(groups: &'a mut GroupsMap) -> GroupsAndImagesMap<'a>
{
    groups.iter_mut().fold(
        GroupsAndImagesMap::new(),
        |mut groups_and_images, (group_name, group)| {
            groups_and_images.insert(group_name.to_string(), GroupAndImages { group, images: vec![] });

            groups_and_images
        },
    )
}

fn match_images_with_groups<'a>(
    groups_and_images: GroupsAndImagesMap<'a>,
    images: &'a Vec<Image>,
) -> RawHelperResult<GroupsAndImagesMap<'a>>
{
    let groups_and_images = images
        .iter()
        .try_fold(groups_and_images, |mut groups_and_images, image| {
            let entry = groups_and_images.entry(image.group.to_string());
            match entry
            {
                Occupied(mut occupied_entry) =>
                {
                    occupied_entry.get_mut().images.push(image);

                    Ok(())
                },
                Vacant(_) => Err(RawHelperError::ImageGroupMissing {
                    path: image.path.to_string(),
                }),
            }?;

            RawHelperResult::Ok(groups_and_images)
        });

    groups_and_images
}
