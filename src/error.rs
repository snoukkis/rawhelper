use std::error::Error;
use std::fmt::Display;
use std::fmt::Formatter;
use std::fmt::Result;
use std::num::ParseFloatError;
use std::num::ParseIntError;
use std::path::PathBuf;

use crate::number::NumberConversionError;

pub type RawHelperResult<T> = std::result::Result<T, RawHelperError>;

#[rustfmt::skip]
#[derive(Debug)]
pub enum RawHelperError
{
    MediaFileMissing { path: PathBuf },
    MediaFileNotFile { path: PathBuf },
    RenameTargetAlreadyExists { path: PathBuf },
    FractionWithBadPartsCount { count: usize },
    ImageMissingMeasuredEV { path: String },
    ImageMissingExposedEV { path: String },
    ImageGroupMissing { path: String },
    _Debug,

    // misc exiftool errors
    ExiftoolNonOneItems { count: usize },
    ExiftoolRunFailure { stderr: String, code: i32 },
    ExiftoolNoFileName { path: PathBuf },
    ExiftoolNoPixelWidth { description: String },

    // missing exiftool tags
    ExiftoolTimestampMissing,
    ExiftoolIsoMissing,
    ExiftoolShutterSpeedMissing,
    ExiftoolMakeAndModelMissing,
    ExiftoolApertureMissing,

    // wrappers
    SerdeJsonError { wrapped: serde_json::Error },
    ParseIntError { wrapped: ParseIntError },
    ParseFloatError { wrapped: ParseFloatError },
    StdIoError { wrapped: std::io::Error },
    GlibError { wrapped: glib::Error },
    TryFromIntError { wrapped: std::num::TryFromIntError },
}

impl From<serde_json::Error> for RawHelperError
{
    fn from(wrapped: serde_json::Error) -> Self { RawHelperError::SerdeJsonError { wrapped } }
}

impl From<ParseFloatError> for RawHelperError
{
    fn from(wrapped: ParseFloatError) -> Self { RawHelperError::ParseFloatError { wrapped } }
}

impl From<std::io::Error> for RawHelperError
{
    fn from(wrapped: std::io::Error) -> Self { RawHelperError::StdIoError { wrapped } }
}

impl From<glib::Error> for RawHelperError
{
    fn from(wrapped: glib::Error) -> Self { RawHelperError::GlibError { wrapped } }
}

impl From<std::num::TryFromIntError> for RawHelperError
{
    fn from(wrapped: std::num::TryFromIntError) -> Self { RawHelperError::TryFromIntError { wrapped } }
}

impl From<NumberConversionError> for RawHelperError
{
    fn from(number_conversion_error: NumberConversionError) -> Self
    {
        match number_conversion_error
        {
            NumberConversionError::ParseIntError { wrapped } => RawHelperError::ParseIntError { wrapped },
            NumberConversionError::FractionWithBadPartsCount { count } =>
            {
                RawHelperError::FractionWithBadPartsCount { count }
            },
        }
    }
}

impl Display for RawHelperError
{
    fn fmt(&self, f: &mut Formatter) -> Result { write!(f, "Got empty array result from exiftool.") }
}

impl Error for RawHelperError {}
