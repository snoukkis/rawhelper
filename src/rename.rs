use std::fs;
use std::fs::Metadata;
use std::io::ErrorKind;
use std::path::Path;
use std::path::PathBuf;

use linked_hash_map::Entry::Occupied;
use linked_hash_map::Entry::Vacant;
use linked_hash_map::LinkedHashMap;
use rayon::iter::ParallelBridge;
use rayon::iter::ParallelIterator;
use walkdir::WalkDir;

use crate::date_path;
use crate::error::RawHelperError;
use crate::error::RawHelperResult;
use crate::path_extension::PathExtension;
use crate::spec::Image;
use crate::spec::Spec;

struct RenamePlan
{
    source_path: PathBuf,
    target_path: PathBuf,
}

/// convert spec.images to map:
/// {
///     "target file name without conflict postfix or extension":
///     [
///         spec.images[...],
///         ...
///     ],
///     ...
/// }
fn group_to_conflict_groups(spec: &mut Spec) -> LinkedHashMap<String, Vec<&mut Image>>
{
    spec.images.iter_mut().fold(
        LinkedHashMap::<String, Vec<&mut Image>>::new(),
        |mut conflict_groups, image| {
            let rename_to = date_path::path_format(image.timestamp);

            match conflict_groups.entry(rename_to)
            {
                Occupied(mut occupied_entry) =>
                {
                    occupied_entry.get_mut().push(image);
                },
                Vacant(vacant_entry) =>
                {
                    vacant_entry.insert(vec![image]);
                },
            }

            conflict_groups
        },
    )
}

/// Find all the files in targeted directories
fn find_all_files(media_directories: Vec<PathBuf>) -> RawHelperResult<Vec<PathBuf>>
{
    let mut files: Vec<PathBuf> = media_directories
        .iter()
        .map(|media_directory| {
            let files_in_media_directory = WalkDir::new(media_directory)
                .into_iter()
                .map(|dir_entry| {
                    let dir_entry = dir_entry?;
                    Ok((dir_entry.metadata()?, dir_entry.path().to_owned()))
                })
                .collect::<Result<Vec<(Metadata, PathBuf)>, walkdir::Error>>()? // collect and return io errors
                .into_iter()
                .filter(|(metadata, _)| metadata.is_file())
                .map(|(_, file)| file)
                .collect::<Vec<PathBuf>>();

            Ok(files_in_media_directory)
        })
        .collect::<Result<Vec<Vec<PathBuf>>, std::io::Error>>()?
        .into_iter()
        .flat_map(|files_in_media_directory| files_in_media_directory.into_iter())
        .collect();

    files.sort();
    files.dedup();

    Ok(files)
}

/// Group files that differ only by directory and extension
fn group_existing_images(files: Vec<PathBuf>) -> RawHelperResult<LinkedHashMap<String, Vec<PathBuf>>>
{
    files.into_iter().try_fold(
        LinkedHashMap::<String, Vec<PathBuf>>::new(),
        |mut existing_images_grouped, file_path| {
            #[allow(unstable_name_collisions)]
            let name = file_path.file_prefix().and_then(|name| name.to_str()).ok_or(
                RawHelperError::ExiftoolNoFileName {
                    path: file_path.to_path_buf(),
                },
            )?;

            match existing_images_grouped.entry(name.to_owned())
            {
                Occupied(mut occupied_entry) =>
                {
                    occupied_entry.get_mut().push(file_path);
                },
                Vacant(vacant_entry) =>
                {
                    vacant_entry.insert(vec![file_path]);
                },
            }

            RawHelperResult::Ok(existing_images_grouped)
        },
    )
}

/// Figure out for each file what the new name should be,
/// usually just timestamp,
/// but sometimes also including postfixes to avoid collisions.
fn build_rename_plans(
    conflict_groups: LinkedHashMap<String, Vec<&mut Image>>,
    mut existing_images_grouped: LinkedHashMap<String, Vec<PathBuf>>,
) -> RawHelperResult<Vec<RenamePlan>>
{
    conflict_groups
        .into_iter()
        .try_fold(Vec::<RenamePlan>::new(), |mut rename_plans, (rename_to, images)| {
            let count = images.len();

            // expand spec files with same timestamp
            for (index, image) in images.into_iter().enumerate()
            {
                let target_file = if count > 1
                {
                    format!("{}_{:0width$}", rename_to, index, width = 3)
                }
                else
                {
                    rename_to.clone()
                };

                let existing_image_files =
                    existing_images_grouped
                        .remove(&image.path)
                        .ok_or(RawHelperError::MediaFileMissing {
                            path: PathBuf::from(&image.path),
                        })?;

                // expand existing files with same name (but not extension)
                for source_path in existing_image_files
                {
                    let target_path = source_path.with_file_name(&target_file);
                    let target_path = match source_path.multi_extension()
                    {
                        Some(extension) => target_path.with_extension(extension),
                        None => target_path,
                    };

                    rename_plans.push(RenamePlan {
                        source_path,
                        target_path,
                    });
                }

                image.path = target_file;
            }

            RawHelperResult::Ok(rename_plans)
        })
}

/// Must abort if any of the target files already exists,
/// assuming it's not itself (source_path === target_path).
fn assert_no_conflict(rename_plans: &Vec<RenamePlan>) -> RawHelperResult<()>
{
    rename_plans
        .iter()
        .par_bridge()
        .map(
            |RenamePlan {
                 source_path,
                 target_path,
                 ..
             }|
             -> RawHelperResult<()> {
                if source_path == target_path
                {
                    Ok(())
                }
                else
                {
                    match target_path.metadata()
                    {
                        Err(error) => match error.kind()
                        {
                            ErrorKind::NotFound => Ok(()),
                            _ => Err(RawHelperError::from(error)),
                        },
                        Ok(_) => Err(RawHelperError::RenameTargetAlreadyExists {
                            path: target_path.to_owned(),
                        }),
                    }
                }
            },
        )
        .collect::<RawHelperResult<()>>()
}

fn rename_files(rename_plans: Vec<RenamePlan>) -> RawHelperResult<()>
{
    // Rename each file to be renamed.
    rename_plans.iter().par_bridge().try_for_each(
        |RenamePlan {
             source_path,
             target_path,
         }|
         -> RawHelperResult<()> {
            if source_path != target_path
            {
                fs::rename(&source_path, &target_path)?;
                println!(
                    "renaming {} to {}",
                    source_path.to_string_lossy(),
                    target_path.to_string_lossy()
                );
            }
            else
            {
                println!("skipping {}", source_path.to_string_lossy());
            }

            Ok(())
        },
    )
}

pub(crate) fn rename(
    input_spec_path: &Path,
    output_spec_path: &Path,
    media_directories: Vec<PathBuf>,
) -> RawHelperResult<()>
{
    let mut spec: Spec = Spec::load_from_file(&input_spec_path)?;

    let conflict_groups = group_to_conflict_groups(&mut spec);

    let files = find_all_files(media_directories)?;

    let existing_images_grouped = group_existing_images(files)?;

    let rename_plans = build_rename_plans(conflict_groups, existing_images_grouped)?;

    assert_no_conflict(&rename_plans)?;

    rename_files(rename_plans)?;

    spec.save_to_file(&output_spec_path)?;

    Ok(())
}
