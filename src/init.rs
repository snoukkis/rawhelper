use std::fs::Metadata;
use std::path::Path;
use std::path::PathBuf;
use std::result::Result;

use rayon::iter::ParallelBridge;
use rayon::iter::ParallelIterator;

use crate::error::RawHelperError;
use crate::error::RawHelperResult;
use crate::exiftool::highlight_tone_priority::HighlightTonePriority;
use crate::exiftool::Exiftool;
use crate::filesystem;
use crate::path_extension::PathExtension;
use crate::spec::Image;
use crate::spec::Spec;

pub(crate) fn init(
    output_spec_path: &Path,
    media_directory: &Path,
    accepted_extensions: Vec<String>,
    date_only: bool,
    pixel_width: Option<f64>,
) -> RawHelperResult<()>
{
    let mut paths = find_paths_to_init(media_directory, accepted_extensions)?;
    paths.sort_by(|a, b| a.cmp(b));

    let mut spec = Spec::new();
    let count = paths.len();

    spec.images = paths
        .into_iter()
        .enumerate()
        .par_bridge()
        .map(|(index, file_path)| {
            println!(
                "processing file {}: {} / {}",
                file_path.to_string_lossy(),
                index + 1,
                count
            );
            process_exif(&file_path, date_only, pixel_width)
        })
        .collect::<Result<Vec<Image>, RawHelperError>>()?;

    spec.images
        .sort_by(|image1, image2| match image1.timestamp.cmp(&image2.timestamp)
        {
            std::cmp::Ordering::Equal => image1.path.cmp(&image2.path),
            different => different,
        });

    spec.save_to_file(&output_spec_path)?;

    Ok(())
}

pub(crate) fn reread_timestamp(
    input_spec_path: &Path,
    output_spec_path: &Path,
    media_directory: &Path,
    accepted_extensions: Vec<String>,
) -> RawHelperResult<()>
{
    let mut spec = Spec::load_from_file(input_spec_path)?;
    let count = spec.images.len();

    spec.images.iter_mut().enumerate().par_bridge().try_for_each(
        |(index, image)| -> Result<(), RawHelperError> {
            let mut file_path = media_directory.to_owned();
            file_path.push(image.path.as_str());

            println!(
                "processing image {}: {} / {}",
                file_path.to_string_lossy(),
                index + 1,
                count
            );

            let file_path = filesystem::disambiguate_path(&file_path, &accepted_extensions)?;

            let tags = Exiftool::load_exif_tags(&file_path)?;

            image.timestamp = tags.find_date_time().ok_or(RawHelperError::ExiftoolTimestampMissing)?;

            Ok(())
        },
    )?;

    spec.images
        .sort_by(|image1, image2| match image1.timestamp.cmp(&image2.timestamp)
        {
            std::cmp::Ordering::Equal => image1.path.cmp(&image2.path),
            different => different,
        });

    spec.save_to_file(&output_spec_path)?;

    Ok(())
}

fn find_paths_to_init(
    directory_path: &Path,
    accepted_extensions: Vec<String>,
) -> Result<Vec<PathBuf>, std::io::Error>
{
    let paths = directory_path
        .read_dir()?
        .map(|dir_entry| {
            let dir_entry = dir_entry?;
            Ok((dir_entry.metadata()?, dir_entry.path()))
        })
        .collect::<Result<Vec<(Metadata, PathBuf)>, std::io::Error>>()? // collect and return io errors
        .into_iter()
        .filter(|(metadata, _)| metadata.is_file())
        .map(|(_, path)| path)
        .filter(|path| filesystem::has_accepted_extension(&path, &accepted_extensions))
        .collect::<Vec<PathBuf>>();

    Ok(paths)
}

fn process_exif(file_path: &Path, date_only: bool, pixel_width: Option<f64>) -> RawHelperResult<Image>
{
    let tags = Exiftool::load_exif_tags(file_path)?;

    let timestamp = tags.find_date_time().ok_or(RawHelperError::ExiftoolTimestampMissing)?;

    #[allow(unstable_name_collisions)]
    let name =
        file_path
            .file_prefix()
            .and_then(|name| name.to_str())
            .ok_or(RawHelperError::ExiftoolNoFileName {
                path: file_path.to_path_buf(),
            })?;

    if date_only
    {
        return Ok(Image::new_date_only(name.to_string(), timestamp));
    };

    let iso = tags.find_iso().ok_or(RawHelperError::ExiftoolIsoMissing)?;
    let mut gain = iso / 100.0;
    let highlight_tone_priority = tags.find_highlight_tone_priority();

    // Fix incorrectly reported ISO while using D+ or D+2
    gain /= match highlight_tone_priority
    {
        HighlightTonePriority::Off => 1.0,
        HighlightTonePriority::DPlus1 | HighlightTonePriority::DPlus2 => 2.0,
    };

    let linear_exposure_time = tags
        .find_shutter_speed()
        .ok_or(RawHelperError::ExiftoolShutterSpeedMissing)?;

    let ev_compensation = tags.find_exposure_compensation().unwrap_or(0.0);

    let aeb_ev_compensation = tags.find_aeb_compensation().unwrap_or(0.0);

    let pixel_width = pixel_width.unwrap_or({
        let make_and_model = tags
            .find_make_and_model()
            .ok_or(RawHelperError::ExiftoolMakeAndModelMissing)?;

        match make_and_model.as_str()
        {
            "Canon - Canon EOS M6" => Ok(3.721042038),
            "Canon - Canon EOS M6 Mark II" => Ok(3.195399211),
            "OnePlus - ONEPLUS A6003" => Ok(1.224241549),
            make_and_model => Err(RawHelperError::ExiftoolNoPixelWidth {
                description: format!("Must provide pixel width in μm for {}", make_and_model),
            }),
        }?
    });

    let pixel_area_ev = 2.0 * pixel_width.log2();

    // snap to exact exposure step
    let gain_ev = rounded_linear_exposure_to_exact_ev(gain, 1.0 / 8.0);
    let time_ev = rounded_linear_exposure_to_exact_ev(linear_exposure_time, 1.0 / 3.0);

    let aperture = tags.find_aperture().ok_or(RawHelperError::ExiftoolApertureMissing)?;
    let aperture_ev = rounded_linear_exposure_to_exact_ev(aperture, 1.0 / 16.0); // 16 = 2*8, because aperture is 2D area

    let exposed_ev = 2.0 * aperture_ev - time_ev - gain_ev;
    let measured_ev = exposed_ev
        + match highlight_tone_priority
        {
            HighlightTonePriority::Off => 0.0,
            HighlightTonePriority::DPlus1 | HighlightTonePriority::DPlus2 => -1.0,
        }
        + ev_compensation
        + aeb_ev_compensation;

    let flash = tags.find_flash_fired();

    let image = Image {
        path: name.to_string(),
        exposed_ev: Some(exposed_ev),
        measured_ev: Some(measured_ev),
        gain_ev: Some(gain_ev),
        pixel_area_ev: Some(pixel_area_ev),
        timestamp,
        flash: Some(flash),
        group: "".to_string(),
    };

    Ok(image)
}

fn rounded_linear_exposure_to_exact_ev(rounded_linear_exposure: f64, step: f64) -> f64
{
    (rounded_linear_exposure.log2() / step).round() * step
}
