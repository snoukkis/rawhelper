# M6 Mark II
~/rusttest/rawhelper/target/debug/rawhelper init -o spec.json -m raw_original -e CR3
~/rusttest/rawhelper/target/debug/rawhelper rename -i spec.json -o spec.json -m ./
~/rusttest/rawhelper/target/debug/rawhelper guess-groups -i spec.json -o spec.json -p raw_converted -e dng
~/rusttest/rawhelper/target/debug/rawhelper calculate-exposure -i spec.json -o spec.json --compensation -2/3
~/rusttest/rawhelper/target/debug/rawhelper write-profiles -i spec.json -p raw_converted -e dng

# Oneplus 6 raw
~/rusttest/rawhelper/target/debug/rawhelper init -o spec.json -m raw_original -e dng
~/rusttest/rawhelper/target/debug/rawhelper reread-timestamp -i spec.json -o spec.json -m jpeg_phone_full_metadata -e jpg
~/rusttest/rawhelper/target/debug/rawhelper rename -i spec.json -o spec.json -m ./
~/rusttest/rawhelper/target/debug/rawhelper guess-groups -i spec.json -o spec.json -p raw_converted -e dng
~/rusttest/rawhelper/target/debug/rawhelper calculate-exposure -i spec.json -o spec.json --compensation +2/3
~/rusttest/rawhelper/target/debug/rawhelper write-profiles -i spec.json -p raw_converted -e dng

# Oneplus 6 jpeg only
~/rusttest/rawhelper/target/debug/rawhelper init -o spec_jpeg_only.json -m jpeg_only -e jpg --date-only
~/rusttest/rawhelper/target/debug/rawhelper rename -i spec_jpeg_only.json -o spec_jpeg_only.json -m ./

# Videos
~/rusttest/rawhelper/target/debug/rawhelper init -o spec_mp4.json -m mp4 -e MP4 --date-only
~/rusttest/rawhelper/target/debug/rawhelper rename -i spec_mp4.json -o spec_mp4.json -m ./
